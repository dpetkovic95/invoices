@extends('base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <!-- /.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"><i class="fa fa-align-justify"></i><strong><h3>Invoices</h3></strong>
                            <div class="btn-group btn-group-toggle" id="radioButtonContainerId" data-toggle="buttons">
                                <label class="btn btn-secondary btn-outline-dark active ">
                                    <input type="radio" name="options" id="option1" value="1" checked> All
                                </label>
                                <label class="btn btn-secondary btn-outline-dark">
                                    <input type="radio" name="options" id="option2" value="2"> Draft
                                </label>
                                <label class="btn btn-secondary btn-outline-dark">
                                    <input type="radio" name="options" id="option3" value="3"> Sent
                                </label>
                                <label class="btn btn-secondary btn-outline-dark">
                                    <input type="radio" name="options" id="option4" value="4"> Paid
                                </label>
                                <label class="btn btn-secondary btn-outline-dark">
                                    <input type="radio" name="options" id="option5" value="5"> Overdue
                                </label>
                            </div>
                            <div class="btn-group btn-group-toggle">
                                <div class="input-group">
                                    <input type="text"
                                           autocomplete="off"
                                           id="datepicker"
                                           name="datefilter"
                                           class="form-control rounded"
                                           placeholder="yyyy-mm-dd"
                                           style="background-image : url('assets/icons/calendar.svg');
                                                    background-Position : 97% center;
                                                    background-Repeat :no-repeat;
                                                    cursor:pointer;"
                                    />
                                    <button type="button"
                                            id="btn-search-date"
                                            class="btn btn-secondary btn-outline-dark">
                                        <svg style="" xmlns="http://www.w3.org/2000/svg"
                                             width="16" height="16"
                                             fill="currentColor"
                                             viewBox="0 0 16 16">
                                            <path
                                                d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="btn-group btn-group-toggle" style="float:right;">
                                <form action="/create" method="get">
                                    <span><button id="btn-create-invoice" class="btn btn-secondary btn-outline-dark">+ Create new</button></span>
                                </form>

                            </div>

                        </div>

                        <div class="card-body">
                            <table id="dtBasicExample" class="table table-responsive-sm table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Invoice ID</th>
                                    <th>Date of issue</th>
                                    <th>Due Date</th>
                                    <th>Client</th>
                                    <th>Total</th>
                                    <th class="no-sort" style="background-image:none; pointer-events: none;"></th>
                                </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data['invoices'] as $invoice)


                                    <tr>
                                        <td><span class="badge badge-success">{{$invoice->status->name}}</span></td>
                                        <td>{{$invoice->id}}</td>
                                        <td>{{$invoice->date_of_issue}}</td>
                                        <td>{{$invoice->due_date}}</td>
                                        <td>{{$invoice->client->name}}</td>
                                        <td>{{$invoice->invoiceItems->first()->total_with_tax}}</td>
                                        <td>
                                            <div class="input-group-prepend">
                                                <button class="btn" type="button"
                                                        data-toggle="dropdown">
                                                    <svg class="c-icon mr-2">
                                                        <use
                                                            xlink:href="{{ url('assets/icons/sprites/free.svg#cil-settings') }}">
                                                        </use>
                                                    </svg>
                                                </button>

                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="#">Edit
                                                        <span class="invoice-settings-icon">
                                                        <svg class="c-icon mr-2">
                                                             <use
                                                                 xlink:href="{{ url('assets/icons/sprites/free.svg#cil-pencil') }}">
                                                             </use>
                                                         </svg>
                                                    </span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">Delete
                                                        <span class="invoice-settings-icon">
                                                          <svg class="c-icon mr-2">
                                                             <use
                                                                 xlink:href="{{ url('assets/icons/sprites/free.svg#cil-trash') }}">
                                                            </use>
                                                          </svg>
                                                    </span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">Send
                                                        <span class="invoice-settings-icon">
                                                        <svg class="c-icon mr-2">
                                                            <use
                                                                xlink:href="{{ url('assets/icons/sprites/free.svg#cil-short-text') }}">
                                                            </use>
                                                        </svg>
                                                    </span>
                                                    </a>
                                                    </a>
                                                    <a class="dropdown-item" href="#">Download
                                                        <span class="invoice-settings-icon">
                                                        <svg class="c-icon mr-2">
                                                            <use
                                                                xlink:href="{{ url('assets/icons/sprites/free.svg#cil-save') }}">
                                                            </use>
                                                        </svg>
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
    </div>


@endsection

@section('javascript')

    <script>
        {
            $(document).ready(function () {
                var table = $('#dtBasicExample').DataTable({
                    lengthMenu: [5, 10, 15, 20, 25, 30],
                    info: false,
                    pagingType: "full_numbers",
                    oLanguage: {
                        sEmptyTable: "Your invoice database has no records",
                        sZeroRecords: "No invoices matching your filter",
                        sProcessing: "Processing request",
                        sInfoThousands: "'"
                    }
                });

                $("#radioButtonContainerId").click(function () {

                    let input = $('input[type=radio]:checked').val()

                    if (input === '1') {
                        table
                            .search('')
                            .draw()
                    } else if (input === '2') {
                        table
                            .search('draft')
                            .draw()
                            .search('')
                    } else if (input === '3') {
                        table
                            .search('sent')
                            .draw()
                            .search('')
                    } else if (input === '4') {
                        table
                            .search('paid')
                            .draw()
                            .search('')
                    } else if (input === '5') {
                        table
                            .search('overdue')
                            .draw()
                            .search('')
                    }
                })
                $('#btn-search-date').on('click', function () {
                    let date = $('#datepicker').val()
                    console.log(date)
                    table
                        .search(date)
                        .draw()
                        .search('');
                })
            })
        }

    </script>

    <script>
        $(document).ready(function () {
            $(function () {
                $("#datepicker").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "c-150:c+30",
                    showButtonPanel: true,
                    closeText: 'Clear',
                    beforeShow: function (input) {
                        setTimeout(function () {
                            $(input).datepicker("widget").find(".ui-datepicker-current").hide();
                            let clearButton = $(input).datepicker("widget").find(".ui-datepicker-close");
                            clearButton.unbind("click").bind("click", function () {
                                $.datepicker._clearDate(input);
                            });
                        }, 1);
                    }
                }).attr("readonly", true).keyup(function (e) {
                    if (e.keyCode === 46 || e.key === "Escape") {
                        $.datepicker._clearDate(this);
                    }
                })
            })
        })
    </script>
@endsection
