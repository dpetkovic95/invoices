@extends('base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <!-- /.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"><i class="fa fa-align-justify"></i><strong><h3>Clients</h3></strong>
                            <div class="btn-group btn-group-toggle" style="float:right;">
                                <span><button id="btn-add-client"
                                              class="btn btn-secondary btn-outline-dark">+ Add new</button></span>
                            </div>
                        </div>

                        <div class="card-body">
                            <table id="dtBasicExample" class="table table-responsive-sm table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Registration number</th>
                                    <th>Tax ID</th>
                                    <th>Address</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Zip code</th>
                                    <th>Current bank account</th>
                                    <th>Phone number</th>
                                    <th>Zip code</th>
                                    <th class="no-sort" style="background-image:none; pointer-events: none;"></th>
                                </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($data['clients'] as $client)
                                    <tr>
                                        <td>{{$client->name}}</td>
                                        <td>{{$client->registration_number}}</td>
                                        <td>{{$client->tax_id}}</td>
                                        <td>{{$client->address}}</td>
                                        <td>{{$client->country}}</td>
                                        <td>{{$client->city}}</td>
                                        <td>{{$client->zip_code}}</td>
                                        <td>{{$client->current_bank_account}}</td>
                                        <td>{{$client->phone_number}}</td>
                                        <td>{{$client->email}}</td>
                                        <td>
                                            <div class="input-group-prepend">
                                                <button class="btn" type="button"
                                                        data-toggle="dropdown">
                                                    <svg class="c-icon mr-2">
                                                        <use
                                                            xlink:href="{{ url('assets/icons/sprites/free.svg#cil-settings') }}">
                                                        </use>
                                                    </svg>
                                                </button>

                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="#">Edit
                                                        <span class="invoice-settings-icon">
                                                        <svg class="c-icon mr-2">
                                                             <use
                                                                 xlink:href="{{ url('assets/icons/sprites/free.svg#cil-pencil') }}">
                                                             </use>
                                                         </svg>
                                                    </span>
                                                    </a>
                                                    <a class="dropdown-item" href="#">Delete
                                                        <span class="invoice-settings-icon">
                                                          <svg class="c-icon mr-2">
                                                             <use
                                                                 xlink:href="{{ url('assets/icons/sprites/free.svg#cil-trash') }}">
                                                            </use>
                                                          </svg>
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
    </div>


@endsection

@section('javascript')

    <script>
        {
            $(document).ready(function () {
                $('#dtBasicExample').DataTable({
                    lengthMenu: [5, 10, 15, 20, 25, 30],
                    info: false,
                    pagingType: "full_numbers",
                    oLanguage: {
                        sEmptyTable: "Your client database has no records",
                        sZeroRecords: "No clients matching your filter",
                        sProcessing: "Processing request"
                    }
                })
            })
        }
    </script>

@endsection
