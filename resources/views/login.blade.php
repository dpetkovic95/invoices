<link href="/css/app.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/app.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="login-reg-panel">
    <div class="login-info-box">
        <h2>Have an account?</h2>
        <p>You can login here</p>
        <label id="label-register" for="log-reg-show">Login</label>
        <input type="radio" name="active-log-panel" id="log-reg-show" checked="checked">
    </div>

    <div class="register-info-box">
        <h2>Don't have an account?</h2>
        <p>You can register here</p>
        <label id="label-login" for="log-login-show">Register</label>
        <input type="radio" name="active-log-panel" id="log-login-show">
    </div>
    <div class="white-panel" id="login">
        <div class="login-show">
            <h2>LOGIN</h2>
            <div class="alert alert-danger" role="alert" id="invalid-credentials" style="display:none; height: 50px">
                The username or password is incorrect !
            </div>
            <input id="login-email" class="form-control" type="email" placeholder="Email">
            <input id="login-password" type="password" placeholder="Password">
            <input type="button" value="Login" onclick="loginSubmit()">
            <a href="#">Forgot password?</a>
        </div>

        <div class="register-show">
            <h2>REGISTER</h2>
            <input id="registration-firstName" type="text" placeholder="Name" required>
            <input id="registration-lastName" type="text" placeholder="Surname" required>
            <input id="registration-email" type="email" placeholder="Email" required>
            <div class="invalid-feedback" id="invalid-feedback-email"
                 style="display:none;width:100%;margin-top:.25rem;margin-bottom:.25rem ;font-size:80%;color:#e55353">
                This email is already in use
            </div>
            <input id="registration-password" type="password" placeholder="Password" required>
            <input id="registration-confirm-password" type="password" placeholder="Confirm Password" required>
            <div class="invalid-feedback-confirm-password" id="invalid-feedback-confirm-password"
                 style="display:none;width:100%;margin-top:.25rem;margin-bottom:.25rem ;font-size:80%;color:#e55353">
                Passwords do not match
            </div>
            <input type="button" value="Register" id="register-btn" onclick="registrationSubmit()">
        </div>
    </div>
</div>

<script>
    function loginSubmit() {
        let data = {};
        data.email = document.getElementById('login-email').value;
        data.password = document.getElementById('login-password').value;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);
                if (response.login === true) {
                    location.replace("/");
                } else {
                    document.getElementById('invalid-credentials').style.display = "block";
                }
            }
        };
        xhttp.open("POST", "/login", true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.send(JSON.stringify(data));
    }

    document.getElementById('login-email').addEventListener('keyup', function (event) {
        if (event.code === 'Enter') {
            loginSubmit();
        }
    });

    document.getElementById('login-password').addEventListener('keyup', function (event) {
        if (event.code === 'Enter') {
            loginSubmit();
        }
    });
</script>

<script>
    function registrationSubmit() {
        let data = {};
        data.firstName = document.getElementById('registration-firstName').value;
        data.lastName = document.getElementById('registration-lastName').value;
        data.email = document.getElementById('registration-email').value;
        data.password = document.getElementById('registration-password').value;
        data.confirmPassword = document.getElementById('registration-confirm-password').value;

        let email = document.getElementById('registration-email');
        let emailPopup = document.getElementById('invalid-feedback-email');
        let password = document.getElementById('registration-password');
        let passwordConfirm = document.getElementById('registration-confirm-password');
        let passwordConfirmPopup = document.getElementById('invalid-feedback-confirm-password');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);

                if (response.email === true && response.password === true) {
                    $(email).addClass("form-control is-valid").removeClass("form-control is-invalid");
                    $(emailPopup).css("display", "none");
                    $(password).removeClass("form-control is-invalid").addClass("form-control is-valid");
                    $(passwordConfirm).removeClass("form-control is-invalid").addClass("form-control is-valid");
                    $(passwordConfirmPopup).css("display", "none");
                    location.replace("/login");
                } else if (response.email === true && response.password === false) {
                    $(document).ready(function () {
                        $(email).removeClass("form-control is-invalid").addClass("form-control is-valid");
                        $(password).addClass("form-control is-invalid").removeClass("form-control is-valid");
                        $(passwordConfirm).addClass("form-control is-invalid").removeClass("form-control is-valid").focus();
                        $(passwordConfirmPopup).css("display", "block");
                        $(emailPopup).css("display", "none");
                    });
                } else if (response.email === false && response.password === true) {
                    $(document).ready(function () {
                        $(password).removeClass("form-control is-invalid").addClass("form-control is-valid");
                        $(email).removeClass("form-control is-valid").addClass("form-control is-invalid").focus();
                        $(passwordConfirm).removeClass("form-control is-invalid").addClass("form-control is-valid");
                        $(passwordConfirmPopup).css("display", "none");
                        $(emailPopup).css("display", "block");
                    });
                } else if (response.email === false && response.password === false) {
                    $(document).ready(function () {
                        $(password).removeClass("form-control is-valid").addClass("form-control is-invalid");
                        $(email).removeClass("form-control is-valid").addClass("form-control is-invalid");
                        $(passwordConfirm).removeClass("form-control is-valid").addClass("form-control is-invalid");
                        $(passwordConfirmPopup).css("display", "block");
                        $(emailPopup).css("display", "block");
                    });
                }
            }
        };
        xhttp.open("POST", "/register", true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.send(JSON.stringify(data));
    }

    document.getElementById('registration-firstName').addEventListener('keyup', function (event) {
        if (event.code === 'Enter') {
            registrationSubmit();
        }
    });

    document.getElementById('registration-lastName').addEventListener('keyup', function (event) {
        if (event.code === 'Enter') {
            registrationSubmit();
        }
    });

    document.getElementById('registration-email').addEventListener('keyup', function (event) {
        if (event.code === 'Enter') {
            registrationSubmit();
        }
    });

    document.getElementById('registration-password').addEventListener('keyup', function (event) {
        if (event.code === 'Enter') {
            registrationSubmit();
        }
    });

    document.getElementById('registration-confirm-password').addEventListener('keyup', function (event) {
        if (event.code === 'Enter') {
            registrationSubmit();
        }
    });
</script>
