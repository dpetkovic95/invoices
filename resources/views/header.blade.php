@php
        $data = new \App\Repositories\UserRepository();
        $name = $data->getUserByEmail(\request()->session()->get('email'));
@endphp
<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar"
            data-class="c-sidebar-show"><span class="c-header-toggler-icon"></span></button>
    <a class="c-header-brand d-sm-none" href="#"><img class="c-header-brand"
                                                      src="{{ asset('assets/brand/coreui-base.svg')}}" width="97"
                                                      height="46" alt="CoreUI Logo"></a>
    <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar"
            data-class="c-sidebar-lg-show" responsive="true"><span class="c-header-toggler-icon"></span></button>
    <ul class="c-header-nav ml-auto mr-4">
        <span> Welcome |  <strong>{{$name['first_name']}}</strong></span>
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link"
               data-toggle="dropdown"
               href="#"
               role="button"
               aria-haspopup="true"
               aria-expanded="false">
                <div class="c-avatar">
                    <svg class="c-icon mr-2">
                        <use xlink:href="{{ url('assets/icons/sprites/brand.svg#cib-gravatar') }}"></use>
                    </svg>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pb-0 pt-0">
                <a class="dropdown-item" href="#">
                    <svg class="c-icon mr-2">
                        <use xlink:href="{{ url('assets/icons/sprites/free.svg#cil-account-logout') }}"></use>
                    </svg>
                    <form action="/logout" method="POST"> @csrf
                        <button type="submit" class="btn btn-block">Logout</button>
                    </form>
                </a>
            </div>
        </li>
    </ul>
</header>

