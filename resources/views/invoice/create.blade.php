@extends('base')

@section('content')

    <div class="card">
        <div class="card-header p-4">
            <h3 class="mb-0">Invoice #{{(new \App\Repositories\InvoiceRepository())->getInvoices()->count()+1}}</h3>
        </div>
    </div>

    <div class="container">
        <div class="card-body-info">
            <div class="row mb-5">
                <div class="col-sm-6">
                    <div class="container">
                        <select id="select-status" class="form-control input-group mb-2 mb-sm-0" aria-label="Default select example"
                                style=" background:transparent; outline: none; border-radius: 10%; width: 50%;">
                            <option disabled selected>Select status</option>
                            @foreach($status as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="userID" value="{{$user->id}}">
                        <select id="select-client" class="form-control input-group mb-2 mb-sm-0"
                                style="background:transparent; outline: none; border-radius: 10%; width:50%;"
                                aria-label="Default select example">
                            <option value="" disabled selected>Select client</option>
                            @foreach($user->clients as $client)
                                <option value="{{$client->id}}">{{$client->name}}</option>
                            @endforeach
                        </select>

                        @foreach($user->clients as $client)
                            <div id="toggle-info-{{$client->id}}" style="display:none; padding-left:20px;">
                                <div><strong>Registration number:</strong> {{$client->registration_number}}</div>
                                <div><strong>Tax ID:</strong> {{$client->tax_id}}</div>
                                <div><strong>Address:</strong> {{$client->address}}</div>
                                <div><strong>Country:</strong> {{$client->country}}</div>
                                <div><strong>City:</strong> {{$client->city}}</div>
                                <div><strong>Zip Code</strong> {{$client->country}}</div>
                                <div><strong>Current Bank Account:</strong> {{$client->current_bank_account}}</div>
                                <div><strong>Phone: </strong> {{$client->phone_number}}</div>
                                <div><strong>Email: </strong> {{$client->email}}</div>
                                <div><strong>Due Date: </strong><input readonly type="text" class="date form-control input-group mb-2 mb-sm-0"
                                                                             id="datepicker-{{$client->id}}"
                                                                             value="yy-mm-dd"
                                                                             style="

                                                                      background-color:transparent;
                                                                      text-align: center; width: 35%;border-color:#d8dbe0; border-radius: 10%">
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <script>
                        $('#select-client').on("change", function () {
                                if ($('#select-client').val() > 0) {
                                    let nodeList = document.querySelectorAll('[id^="toggle-info"]');
                                    nodeList.forEach(node => {
                                        node.style.display = "none"
                                    });
                                    $('#toggle-info-' + $('#select-client').val()).css("display", "block");
                                } else {
                                    $('#toggle-info-' + $('#select-client').val()).css("display", "none");
                                }
                            }
                        )
                    </script>

                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="tab_logic">
                    <thead>
                    <tr>
                        <th class="text-center"> #</th>
                        <th class="text-center"> Product</th>
                        <th class="text-center"> Qty</th>
                        <th class="text-center"> Price</th>
                        <th class="text-center"> Tax</th>
                        <th class="text-center"> Total</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12">
                <input type="button" id="add_row" class="btn btn-secondary btn-success" value="Add">
            </div>
        </div>
        <div class="row clearfix" style="margin-top:20px">
            <div class="pull-right col-md-4">
                <table class="table table-bordered table-hover" id="tab_logic_total">
                    <tbody>
                    <tr>
                        <th class="text-center">Grand Total</th>
                        <td class="text-center"><input type="number" name='total_amount' id="total_amount"
                                                       placeholder='0.00' class="form-control" readonly/></td>
                    </tr>

                    </tbody>
                </table>
                <button id="btn-create-invoice" onclick="createInvoice()" type="button" class="btn btn-primary">Create
                </button>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        {

            $(document).ready(function () {
                window.globalCounter = 1;
                $("#add_row").click(function () {
                    let data = '<tr>'+
                        '<td>'+ window.globalCounter +'</td>' +
                        '<td><input id="product" type="text" name="product[]" placeholder="Enter Product Name" class="form-control"/></td> ' +
                        '<td><input type="number" id="qty" name="qty[]" placeholder="Enter Quantity" class="form-control qty" step="0" min="0"/></td> ' +
                        '<td><input type="number" name="price[]" placeholder="Enter Unit Price" class="form-control price" step="0.00" min="0"/></td> ' +
                        '<td><input type="number" name="tax[]" placeholder="Enter unit tax" class="form-control tax" step="0" value="0" min="0"/></td> ' +
                        '<td><input type="number" name="total[]" placeholder="0" class="form-control total" readonly/></td> ' +
                        '<td><button id="delete_row" class="btn btn-ghost-danger" type="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash"viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/> <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg></button></td> ' +
                        '</tr>';
                    $("#tab_logic").append(data);
                    window.globalCounter++;
                })

                $("#tab_logic tbody").on("click", "#delete_row", function (ev) {
                    let $currentTableRow = $(ev.currentTarget).parents('tr')[0];
                    $currentTableRow.remove();
                    window.globalCounter--;

                    $('#tab_logic').find('tr').each(function (index) {
                        let firstTDDomEl = $(this).find('td')[0];
                        let $firstTDJQObject = $(firstTDDomEl);
                        $firstTDJQObject.text(index);
                        calc();
                        calc_total();
                    });
                });


                $('#tab_logic tbody').on('keyup change', function () {
                    calc();
                });
                $('#tax').on('keyup change', function () {
                    calc_total();
                });


                function calc() {
                    $('#tab_logic tbody tr').each(function (i, element) {
                        let html = $(this).html();
                        if (html !== '') {
                            let qty = $(this).find('.qty').val();
                            let price = $(this).find('.price').val();
                            let tax = $(this).find('.tax').val();
                            $(this).find('.total').val(parseFloat(qty) * ((parseFloat(price)) +(parseFloat(tax))));
                            calc_total();
                        }
                    });
                }

                function calc_total() {
                    total = 0;
                    $('.total').each(function () {
                        total += parseInt($(this).val());
                    });
                    $('#sub_total').val(total.toFixed(2));
                    tax_sum = total / 100 * $('#tax').val();
                    $('#tax_amount').val(tax_sum.toFixed(2));
                    $('#total_amount').val((total).toFixed(2));
                }
            })
        }
    </script>
    <script>
        $(document).ready(function () {
            $(function () {
                $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "c-150:c+30",
                    showButtonPanel: true,
                    closeText: 'Clear',
                    beforeShow: function (input) {
                        setTimeout(function () {
                            $(input).datepicker("widget").find(".ui-datepicker-current").hide();
                            let clearButton = $(input).datepicker("widget").find(".ui-datepicker-close");
                            clearButton.unbind("click").bind("click", function () {
                                $.datepicker._clearDate(input);
                            });
                        }, 1);
                    }
                }).keyup(function (e) {
                    if (e.keyCode === 46 || e.key === "Escape") {
                        $.datepicker._clearDate(this);
                    }
                })
            })
        })


    </script>

    <script>
        {
            function createInvoice() {
                let selectedClient = $('#select-client').find(":selected").val(), payload = {};

                let invoiceItems = [],
                    invoiceItemsElements = $('#tab_logic tbody')[0].children;
                let item, description, quantity, total, price, tax;

                for (let element of invoiceItemsElements) {
                    item = {};
                    description = element.querySelector('[name="product[]"]').value;
                    quantity = element.querySelector('[name="qty[]"]').value;
                    price = element.querySelector('[name="price[]"]').value;
                    tax = element.querySelector('[name="tax[]"]').value;
                    total = element.querySelector('[name="total[]"]').value;

                    item.description = description;
                    item.quantity = quantity;
                    item.price = price;
                    item.tax = tax;
                    item.total = total;
                    invoiceItems.push(item);
                }

                payload.user = $('#userID').val();
                payload.client = selectedClient;
                payload.status = $('#select-status').val();
                payload.dueDate = $('#datepicker-' +selectedClient).val();
                payload.invoiceItems = invoiceItems;

                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        window.location.replace('/');
                    }
                };
                xhttp.open("POST", "/create", false);
                xhttp.setRequestHeader('Content-Type', 'application/json');
                xhttp.send(JSON.stringify(payload));
            }
        }
    </script>

@endsection
