<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function addUser($firstName, $lastName, $email, $password)
    {
        User::insert([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password' => Hash::make($password)
        ]);
    }
    public function getUserByEmail($email) {
        return User::where('email',$email)->first();
    }
}
