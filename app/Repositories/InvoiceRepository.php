<?php

namespace App\Repositories;

use App\Models\Invoice;
use App\Models\InvoiceItem;
use DateTime;

class InvoiceRepository
{
    public function getInvoices()
    {
        $user = (new UserRepository())->getUserByEmail(request()->session()->get('email'));
        return Invoice::all()->where('user_id', $user->id);
    }

    public function createInvoice(int $clientId, int $userId, int $statusId, string $dueDate)
    {
        $invoice = new Invoice();
        $invoice->client_id = $clientId;
        $invoice->user_id = $userId;
        $invoice->status_id = $statusId;
        $invoice->date_of_issue = new DateTime();
        $invoice->due_date = DateTime::createFromFormat('Y-m-d', $dueDate);
        $invoice->save();

        return $invoice->id;
    }

    public function createInvoiceItems(int $invoiceId, array $invoiceItems)
    {
        foreach ($invoiceItems as $item) {
            $invoiceItem = new InvoiceItem();
            $invoiceItem->invoice_id = $invoiceId;
            $invoiceItem->description = $item['description'];
            $invoiceItem->quantity = $item['quantity'];
            $invoiceItem->price = $item['price'];
            $invoiceItem->total_no_tax = $item['quantity'] * $item['price'];
            $invoiceItem->tax = $item['tax'];
            $invoiceItem->total_with_tax = $item['quantity'] * ($item['price'] + $item['tax']);
            $invoiceItem->save();
        }
    }
}
