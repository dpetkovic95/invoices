<?php

namespace App\Repositories;

use App\Models\Status;

class StatusRepository
{
    public function getStatuses() {
        return Status::all();
    }
}
