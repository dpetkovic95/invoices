<?php

namespace App\Http\Controllers;

use App\Repositories\InvoiceRepository;

class HomeController extends Controller
{
    public function renderHome()
    {
        $data['invoices'] = (new InvoiceRepository())->getInvoices();
        return view('home', $data)->with('data', $data);
    }
}
