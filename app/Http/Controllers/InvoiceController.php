<?php

namespace App\Http\Controllers;

use App\Repositories\InvoiceRepository;
use App\Repositories\StatusRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class InvoiceController
{
    public function renderCreateInvoice()
    {
        $data['user'] = (new UserRepository())->getUserByEmail(\request()->session()->get('email'));
        $data['status'] = (new StatusRepository())->getStatuses();
        return view('invoice/create', $data);
    }

    public function createInvoice(Request $request)
    {
        $payload = json_decode($request->getContent(), true);
        $invoiceRepository = new InvoiceRepository();
        $invoiceID = $invoiceRepository->createInvoice($payload['client'], $payload['user'], $payload['status'], $payload['dueDate']);
        $invoiceRepository->createInvoiceItems($invoiceID, $payload['invoiceItems']);
        return response()->json(['success' => true]);
    }
}
