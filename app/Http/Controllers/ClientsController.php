<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;

class ClientsController extends Controller
{
    public function renderClients()
    {
        $user = (new UserRepository())->getUserByEmail(request()->session()->get('email'));
        $data['clients'] = $user->clients;
        return view('clients/list', $data)->with('data', $data);
    }
}
