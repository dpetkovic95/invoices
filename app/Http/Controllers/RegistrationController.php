<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function handleRegistration(Request $request)
    {
        $firstName = $request->input('firstName');
        $lastName = $request->input('lastName');
        $email = $request->input('email');
        $password = $request->input('password');
        $passwordConfirm = $request->input('confirmPassword');

        $exists = User::where([
            ['email', '=', $email]
        ])->first();

        if (!$exists && $password === $passwordConfirm && $firstName) {
            $userRepository = new UserRepository();
            $userRepository->addUser($firstName, $lastName, $email, $password);
            return response()->json(array("email" => true, "password" => true));
        } else if (!$exists && $password !== $passwordConfirm) {
            return response()->json(array("email" => true, "password" => false));
        } else if ($exists && $password === $passwordConfirm) {
            return response()->json(array("email" => false, "password" => true));
        } else {
            return response()->json(array("email" => false, "password" => false));
        }
    }
}
