<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function renderLogin()
    {
        return view('login');
    }

    public function handleLogin(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');


        $user = User::where([
            ['email', '=', $email],
        ])->first();

        if ($user && Hash::check($password, $user->password)) {
            $request->session()->put('email', $email);
            return response()->json(array("login" => true));
        }

        return response()->json(array("login" => false));
    }

    public function logout(Request $request)
    {
        $request->session()->forget('key');
        return redirect('/login');
    }
}
