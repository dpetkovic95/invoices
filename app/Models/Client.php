<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $registration_number
 * @property int $tax_id
 * @property string $address
 * @property string $country
 * @property string $city
 * @property string $zip_code
 * @property string $current_bank_account
 * @property string $phone_number
 * @property string $email
 */
class Client extends Model
{
    protected $table = 'clients';
    protected $primaryKey = 'id';


}
