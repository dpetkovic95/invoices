<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $invoice_id
 * @property string $description
 * @property int $quantity
 * @property string $price
 * @property string $total_no_tax
 * @property string $tax
 * @property string $total_with_tax
 */

class InvoiceItem extends Model
{
    protected $table = 'invoice_items';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'id','id');
    }
}
