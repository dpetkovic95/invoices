<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $client_id
 * @property int $user_id
 * @property int $status_id
 * @property \DateTime $date_of_issue
 * @property \DateTime $due_date
 */
class Invoice extends Model
{
    protected $table = 'invoices';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function invoiceItems()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
