<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property boolean $is_active
 *
 */
class User extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';


    public function clients()
    {
        return $this->belongsToMany(
            Client::class,
            'user_client',
            'user_id',
            'client_id'
        );
    }

    public function invoices()
    {
        return $this->hasMany(
            Invoice::class,
            'user_id',
            'id'
        );
    }

}
