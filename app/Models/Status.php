<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class Status extends Model
{
    protected $table = 'statuses';
    protected $primaryKey = 'id';

    public function status()
    {
        return $this->hasMany(
            Invoice::class,
            'invoice_id',
            'id'
        );
    }
}
