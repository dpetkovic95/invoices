const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'resources/js/app.js',
], 'public/js/app.js');

mix.scripts([
    'resources/js/coreui.bundle.min.js',
], 'public/js/coreui.bundle.min.js');

mix.scripts([
    'resources/js/coreui-chartjs.bundle.js',
], 'public/js/coreui-chartjs.bundle.js');

mix.scripts([
    'resources/js/coreui-utils.js',
], 'public/js/coreui-utils.js');

mix.styles([
    'resources/css/app.css',
], 'public/css/app.css');

mix.styles([
    'resources/css/style.css',
], 'public/css/style.css');

mix.styles([
    'resources/css/flag.min.css',
], 'public/css/flag.min.css');

mix.styles([
    'resources/css/free.min.css',
], 'public/css/free.min.css');

