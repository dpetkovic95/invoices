<?php

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'renderHome'])->middleware('auth');

Route::get('/create', [InvoiceController::class, 'renderCreateInvoice']);
Route::post('/create', [InvoiceController::class, 'createInvoice']);

Route::get('/clients', [ClientsController::class, 'renderClients',]);

Route::get('/login', [LoginController::class, 'renderLogin']);
Route::post('/login', [LoginController::class, 'handleLogin']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::post('/register', [RegistrationController::class, 'handleRegistration']);

Route::get('/test', [TestController::class, 'test']);
